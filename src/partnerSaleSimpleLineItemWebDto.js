/**
 * @class {PartnerSaleSimpleLineItemWebDto}
 */
export default class PartnerSaleSimpleLineItemWebDto{

    _serialNumber:string;

    constructor(
        serialNumber:string
    ){

        this._serialNumber = serialNumber;

    }

    /**
     * getter methods
     */
    get serialNumber():string{
        return this._serialNumber;
    }

    toJSON() {
        return {
            serialNumber: this._serialNumber
        }
    }

}

