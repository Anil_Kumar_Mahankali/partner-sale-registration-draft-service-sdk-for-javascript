import ExtendedWarrantyRequestRuleCompositeSerialCodeWebView from './extendedWarrantyRequestRuleCompositeSerialCodeWebView';
import ExtendedWarrantyRequestRuleSimpleSerialCodeWebView from './extendedWarrantyRequestRuleSimpleSerialCodeWebView';

/**
 * @class {ExtendedWarrantyRequestRuleWebView}
 */
export default class ExtendedWarrantyRequestRuleWebView {

    _term:string;

    _simpleSerialCode:ExtendedWarrantyRequestRuleSimpleSerialCodeWebView[];

    _compositeSerialCode:ExtendedWarrantyRequestRuleCompositeSerialCodeWebView[];

    constructor(
        term:string,
        simpleSerialCode:ExtendedWarrantyRequestRuleSimpleSerialCodeWebView[],
        compositeSerialCode:ExtendedWarrantyRequestRuleCompositeSerialCodeWebView[]
    ) {

        this._term = term;

        this._simpleSerialCode = simpleSerialCode;

        this._compositeSerialCode = compositeSerialCode;
    }

    get term():string {
        return this._term;
    }

    get simpleSerialCode():ExtendedWarrantyRequestRuleSimpleSerialCodeWebView[] {
        return this._simpleSerialCode;
    }

    get compositeSerialCode():ExtendedWarrantyRequestRuleCompositeSerialCodeWebView[] {
        return this._compositeSerialCode;
    }


    toJSON(){
        return {
            term: this._term,
            simpleSerialCode: this._simpleSerialCode,
            compositeSerialCode: this._compositeSerialCode
        }
    }
}
