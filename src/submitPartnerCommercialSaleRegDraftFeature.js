import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import PartnerCommercialSaleRegDraftView from './partnerCommercialSaleRegDraftView';
import SubmitPartnerSaleRegDraftDto from './submitPartnerSaleRegDraftDto';
import PartnerSaleRegistrationDraftServiceSdkConfig from './partnerSaleRegistrationDraftServiceSdkConfig';

@inject(PartnerSaleRegistrationDraftServiceSdkConfig, HttpClient)
class SubmitPartnerCommercialSaleRegDraftFeature {

    _config:PartnerSaleRegistrationDraftServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:PartnerSaleRegistrationDraftServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     *
     * @param {SubmitPartnerSaleRegDraftDto} request
     * @param {string} accessToken
     * @returns {Promise.<PartnerCommercialSaleRegDraftView>}
     */
    execute(request:SubmitPartnerSaleRegDraftDto,
            accessToken:string):Promise<PartnerCommercialSaleRegDraftView> {

        return this._httpClient
            .createRequest(`partner-sale-registration/submitpartnersaleregdraft`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);
    }
}

export default SubmitPartnerCommercialSaleRegDraftFeature;
