import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import PartnerSaleRegistrationDraftServiceSdkConfig from './partnerSaleRegistrationDraftServiceSdkConfig';
import UpdateSaleInvoiceUrlOfPartnerSaleRegDraftReq from './updateSaleInvoiceUrlOfPartnerSaleRegDraftReq';

@inject(PartnerSaleRegistrationDraftServiceSdkConfig, HttpClient)
class UpdateSaleInvoiceUrlOfPartnerSaleRegDraftFeature {

    _config:PartnerSaleRegistrationDraftServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:PartnerSaleRegistrationDraftServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     *
     * @param request
     * @param {UpdateSaleInvoiceUrlOfPartnerSaleRegDraftReq} request
     * @param {string} accessToken
     */
    execute(request:UpdateSaleInvoiceUrlOfPartnerSaleRegDraftReq,
            accessToken:string) {

        return this._httpClient
            .createRequest(`partner-sale-registration/${request.id}/updateinvoiceurl`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request.invoiceUrl)
            .send();

    }
}

export default UpdateSaleInvoiceUrlOfPartnerSaleRegDraftFeature;
