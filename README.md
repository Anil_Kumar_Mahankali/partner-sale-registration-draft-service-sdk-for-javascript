## Description
Precor Connect partner sale registration draft service SDK for javascript.


## Setup

**install via jspm**  
```shell
jspm install partner-sale-registration-draft-service-sdk=bitbucket:precorconnect/partner-sale-registration-draft-service-sdk-for-javascript
```

**import & instantiate**
```javascript
import PartnerSaleRegistrationDraftServiceSdk,{PartnerSaleRegistrationDraftServiceSdkConfig} from 'partner-sale-registration-draft-service-sdk'

const partnerSaleRegistrationDraftServiceSdkConfig =
    new PartnerSaleRegistrationDraftServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const partnerSaleRegistrationDraftServiceSdk =
    new PartnerSaleRegistrationDraftServiceSdk(
        partnerSaleRegistrationDraftServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```